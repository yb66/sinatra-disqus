## Sinatra Disqus ##

Drop in disqus to any Sinatra page via a handy helper.

### Installation ###

    gem install sinatra-disqus
    
or via Bundler:

    gem "sinatra-disqus"


### Example ###

Note: It used to be that you were able to put Disqus into "development mode", [this is no longer valid](http://iainbarnett.me.uk/articles/disqus-developer-mode). Instead, add another site shortname and modify the settings slightly, similar to the examples below.

#### Classic style

    require 'sinatra'
    require 'haml'
    require 'sinatra/disqus'
      
    configure :production do
      # Disqus settings
      set :disqus_shortname, "example.org"
    end

    configure :development, :test do
      set :disqus_shortname, "dev-example.org"
    end

    get "/" do
      @title = "Home"
      haml :home
    end

    __END__

    @@ layout
    !!!
    %body
      = yield

    @@ home
    #main
      %p
        This is the home page.
      %p
        Now for some comments:
    #comments
      = inject_disqus

#### Modular style ####

    require 'sinatra/base' # for modular apps
    require 'haml'
    require 'sinatra/disqus'
    
    class Example < Sinatra::Base
      register Sinatra::Disqus # because it's modular
      
      configure do
        enable :inline_templates # just for this example
      end

      configure :production do
        # Disqus settings
        set :disqus_shortname, "example.org"
      end

      configure :development do
        # Disqus settings
        set :disqus_shortname, "dev-example.org"
      end
      
      get "/" do
        @title = "Home"
        haml :home
      end
    end

    __END__

    @@ layout
    !!!
    %body
      = yield

    @@ home
    #main
      %p
        This is the home page.
      %p
        Now for some comments:
    #comments
      = inject_disqus # you'd probably do this at the bottom of a view instead.

### Version numbering ###

This project attempts to follow the [semver](http://semver.org/) standard of numbering.

### Licence ###

Look in the LICENCE file.