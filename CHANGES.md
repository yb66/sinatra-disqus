## CH-CH-CH-CH-CHANGES! ##

### Wednesday the 4th of November 2015, v2.1.0 ###

* Added setting for host, as an alternative to the one in the request. Useful when running on localhost but still want the request to output a domain.
* Chopped up the script into smaller pieces to make dealing with it easier (internal).

----


### Friday the 28th of June 2013, v2.0.0 ###

* Disqus has dropped support for developer mode. Updated to reflect this.
* Updated scripts.
* 100% coverage for documentation.

----


### Saturday the 20th of April 2013, v1.1.0 ###

* More updating of docs.
* Removed reliance on Haml.

----


### Saturday the 20th of April 2013, v1.0.0 ###

* Updated the docs a bit.
* Bumped to v1.0.0 for semver.

----


### Thursday the 9th of August 2012, v0.0.1 ###

* First release.
* It has a spec!

----
