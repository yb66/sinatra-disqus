# encoding: UTF-8

require 'sinatra/base'

# @see http://www.sinatrarb.com/
module Sinatra

  # Drop in disqus to any Sinatra view via a handy helper. 
  module Disqus

    # Top part of the javascripts.
    Disqus_output_top = <<TOP
<div id='disqus_thread'></div>
<script type='text/javascript'>
  //<![CDATA[
TOP

    Disqus_output_function_top = <<FUNCTOP
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
FUNCTOP

    Disqus_output_function_bot = <<FUNCBOT
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
  //]]>
FUNCBOT

    # The bottom bit of the sandwich we're making.
    Disqus_output_bottom = <<BOTTOM
</script>
<noscript>
  Please enable JavaScript to view the <a href="//disqus.com/?ref_noscript">comments powered by Disqus.</a>
</noscript>
<a href="//disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
BOTTOM

    # @private
    # Private yet helpful methods.
    module Private
      require 'digest/md5'
    
      # Unique (repeatable) identifier for page.
      # @param [String] The page uri.
      def self.disqus_identifier( request_path )
        Digest::MD5.hexdigest(request_path)
      end


      # @param [Hash] opts
      # @option opts [String] :title The page title, usually held in @title
      # @option opts [String] :disqus_shortname The short name of the site (e.g. example.org )
      # @option opts [String] :disqus_identifier The unique identifier the page the comments sit on.
      # @option opts [String] :request_url The page's url.
      # @return [String] The portion of the disqus javascript with the vars.
      # @note The option :disqus_developer is no longer supported by Disqus
      def self.build_vars( opts )
<<STR
    var disqus_title = "#{opts[:title].gsub(/'/, "\\\\'")}";
    var disqus_shortname = '#{opts[:disqus_shortname]}';
    var disqus_identifier = '#{opts[:disqus_identifier]}';
    var disqus_url = '#{opts[:request_url]}';
STR
      end

    end


    # Handy helpers.
    module Helpers
      
      # @return [String] Returns the bit of HTML/javascript to stick in the page.
      def inject_disqus
        req_url = settings.disqus_host ?
          File.join(settings.disqus_host,request.path_info) :
          request.url

          options = {
            request_url: req_url,
            disqus_shortname: settings.disqus_shortname,
            title: @title,
            disqus_identifier: Private.disqus_identifier(request.path)
          }
          "#{Disqus_output_top}#{Private.build_vars(options)}#{Disqus_output_function_top}        dsq.src = '//#{settings.disqus_shortname}.disqus.com/embed.js';\n#{Disqus_output_function_bot}#{Disqus_output_bottom}"
      end

    end


    # Sinatra hook for extensions.
    def self.registered(app)
      app.helpers Disqus::Helpers

      # @param [String] s The short name of the site (e.g. example.org )
      app.set :disqus_shortname, nil

      # @param [String] s Alternative host to the one in the request. Useful when running on localhost but still want the request to output a domain.
      # @example
      #   set :disqus_host, "//example.org"
      app.set :disqus_host, nil

    end # registered
  end
  register Disqus
end
