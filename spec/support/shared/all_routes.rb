# encoding: UTF-8

require 'sinatra/base'
require_relative "../../../lib/sinatra/Disqus.rb"

class Example < Sinatra::Base
  register Sinatra::Disqus
  
  configure do
    set :disqus_shortname, "example.org"
  end
  
  get "/" do
    @title = "Home"
    inject_disqus
  end
end

shared_context "All pages" do
  include Rack::Test::Methods
  let(:app){ Example }
end

shared_examples_for "Any route" do
  subject { last_response }
  it { should be_ok }
end