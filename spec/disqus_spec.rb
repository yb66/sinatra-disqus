# encoding: UTF-8

require "rspec_helper"

require_relative "../lib/sinatra/Disqus.rb"

describe "Disqus" do
  context "General" do
    before{ get '/' }
    include_context "All pages"
    it_should_behave_like "Any route"
    context "Output" do
      let(:expected) { <<STR
<div id='disqus_thread'></div>
<script type='text/javascript'>
  //<![CDATA[
    var disqus_title = "Home";
    var disqus_shortname = 'example.org';
    var disqus_identifier = '6666cd76f96956469e7be39d750cc7d9';
    var disqus_url = 'http://example.org/';
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//example.org.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
  //]]>
</script>
<noscript>
  Please enable JavaScript to view the <a href="//disqus.com/?ref_noscript">comments powered by Disqus.</a>
</noscript>
<a href="//disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
STR
      }
      subject{ last_response.body }
      it { should == expected }
    end
  end

  context "settings.disqus_host" do
    include Rack::Test::Methods
    def app
      app = Sinatra.new do
        register Sinatra::Disqus
  
        configure do
          set :disqus_shortname, "example.org"
          set :disqus_host, "//localhost:4567"
        end
  
        get "/" do
          @title = "Home"
          inject_disqus
        end
      end
    end
    before{ get '/' }
    context "Output" do
      let(:expected) { <<STR
<div id='disqus_thread'></div>
<script type='text/javascript'>
  //<![CDATA[
    var disqus_title = "Home";
    var disqus_shortname = 'example.org';
    var disqus_identifier = '6666cd76f96956469e7be39d750cc7d9';
    var disqus_url = '//localhost:4567/';
    (function() {
        var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
        dsq.src = '//example.org.disqus.com/embed.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
    })();
  //]]>
</script>
<noscript>
  Please enable JavaScript to view the <a href="//disqus.com/?ref_noscript">comments powered by Disqus.</a>
</noscript>
<a href="//disqus.com" class="dsq-brlink">comments powered by <span class="logo-disqus">Disqus</span></a>
STR
      }
      subject{ last_response.body }
      it { should == expected }
    end
    
  end
end
