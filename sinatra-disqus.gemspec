# -*- encoding: utf-8 -*-
require File.expand_path('../lib/sinatra/disqus/version', __FILE__)

Gem::Specification.new do |gem|
  gem.authors       = ["Iain Barnett"]
  gem.email         = ["iainspeed@gmail.com"]
  gem.description   = %q{Drop in disqus to any Sinatra view via a handy helper.}
  gem.summary       = %q{Easy disqus for Sinatra.}
  gem.homepage      = "https://bitbucket.org/yb66/sinatra-disqus"

  gem.files         = `git ls-files`.split($\)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.name          = "sinatra-disqus"
  gem.require_paths = ["lib"]
  gem.version       = Sinatra::Disqus::VERSION
  gem.add_dependency( "sinatra" )
end
